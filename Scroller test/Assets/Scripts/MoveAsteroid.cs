﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAsteroid : MonoBehaviour {
    float move;

    private void Start()
    {
        move = Random.Range(-1f, 1f);
    }

    void Update () {
        gameObject.transform.Translate(new Vector3(move * Time.deltaTime, -1 * GenerationEnemies.speed * Time.deltaTime, 0));
	}
}
