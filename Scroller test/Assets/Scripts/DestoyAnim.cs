﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoyAnim : MonoBehaviour {
    private float time = 1;

    private void Start()
    {
        GetComponent<AudioSource>().Play();
    }

    void Update () {
        time -= Time.deltaTime;
        if(time <= 0)
        {
            Destroy(gameObject);
        }
	}
}
