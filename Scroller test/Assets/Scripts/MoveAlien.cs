﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAlien : MonoBehaviour {
    float move;

    void Update()
    {
        move = Mathf.Cos(gameObject.transform.position.y) * 2f;
        
        gameObject.transform.Translate(new Vector3(move * Time.deltaTime, -1 * GenerationEnemies.speed * Time.deltaTime, 0));
    }
}
