﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerationEnemies : MonoBehaviour {
    private const float CONST_TIME = 15;//время, через которое изменяется частота появления и передвижения астероидов
    public GameObject[] enemyes;//массив префабов врагов
    private Vector3 vect;//размеры камеры
    public float time = 3;//стартовая частота появления врагов
    private float timer = CONST_TIME;//счетчик прошедшего времени. Если == 0, значит прошло 15 сек
    public static float speed = 1;//
    public Text speedText;
    private int speedGame = 2;

    void Start () {
        vect = Camera.main.ViewportToWorldPoint(Camera.main.transform.position);
        StartCoroutine(generation());
        speed = 1;
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0 )
        {
            if(time > 0.5f)
            {
                time -= 0.3f;
            }
            else
            {
                if(time > 0.1f)
                {
                    time -= 0.05f;
                }
            }
            speed += 0.2f;
            speedGame++;
            speedText.text = "Speed: " + speedGame;
            timer = CONST_TIME;
        }
    }

    IEnumerator generation()
    {
        
        while (true)
        {
            yield return new WaitForSeconds(time);

            selectNextEnemy();
            
        }
    }

   

    private void selectNextEnemy()
    {
        Vector3 randomPos = new Vector3(Random.Range(vect.x + 0.7f, -vect.x - 0.7f), 7, 3);
        GameObject obj;
        int i = 0;

        if(Random.value < 0.85f)
        {
            i = 0;
        }
        else {
            i = 1;
        }
        
        obj = Instantiate(enemyes[i], randomPos, Quaternion.identity);
        obj.transform.parent = gameObject.transform;
        obj.transform.localScale = new Vector3(1, 1, 0);
    }
}
