﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControll : MonoBehaviour {
    private float move = 0;
    public float speed;
    public GameObject bullet;
    private Vector3 vect;

    void Start () {
        vect = Camera.main.ViewportToWorldPoint(Camera.main.transform.position);// вычисляем ширину экрана для ограничения перемещения игрока
       
    }

    void Update () {
        move = Input.GetAxis("Horizontal");

        if(move > 0 && gameObject.transform.position.x < -vect.x - 0.7f)
        {
            gameObject.transform.Translate(Vector3.right * speed * move * Time.deltaTime);
        }

        if (move < 0 && gameObject.transform.position.x > vect.x + 0.7f)
        {
            gameObject.transform.Translate(Vector3.right * speed * move * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space) )
        {
            if (!ButtonPause.getKey())
            {
                GameObject obj;
                obj = Instantiate(bullet, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1, 3), Quaternion.identity);
                obj.transform.parent = gameObject.transform.parent;
                obj.transform.localScale = new Vector3(1, 1, 0);
                GetComponent<AudioSource>().Play();
            }
            

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        SceneManager.LoadScene("Menu");
    }
    

    
}
