﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonsMenu : MonoBehaviour {
    
    public void start()
    {
        GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("Game");
       
    }

    public void connect()
    {
        GetComponent<AudioSource>().Play();
       
        
    }

	public void exit()
    {
        GetComponent<AudioSource>().Play();
        Application.Quit();
    }
}
