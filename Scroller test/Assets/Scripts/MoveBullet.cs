﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveBullet : MonoBehaviour {
    private float speed = 4;
    private Text textCount;
    private float count = 0;
    

    public GameObject exp;

    

    private void Start()
    {   
        textCount =  GameObject.Find("UI/Count").GetComponent<Text> ();       
    }

    void Update()
    {
        
        gameObject.transform.Translate(Vector3.up * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag != "Wall")
        {
            count = int.Parse(textCount.text) + 1;
            textCount.text = count.ToString();

            Instantiate(exp, collision.gameObject.transform.position, Quaternion.identity);
           

            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }

   
   
}
