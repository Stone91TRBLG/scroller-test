﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonPause : MonoBehaviour {
    private static bool key = false;
    public Text text;
    


    private void OnMouseDown()
    {
        gameObject.transform.localScale = new Vector3(1.2f, 1.2f, 0);
        GetComponent<AudioSource>().Play();
    }

    private void OnMouseUp()
    {
        gameObject.transform.localScale = new Vector3(1f, 1f, 0);
        pause();
    }

    private void pause()
    {
        key = !key;

        if (key)
        {
            
            Time.timeScale = 0;
            text.gameObject.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            text.gameObject.SetActive(false);
        }
        
    }

    public static bool getKey()
    {
        return key;
    }
}
